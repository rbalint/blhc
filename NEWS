NEWS
====

Version 0.09
------------

- Detect restore of -D_FORTIFY_SOURCE=2 after it was overwritten by
  -D_FORTIFY_SOURCE=0 or 1 or -U_FORTIFY_SOURCE; reported by Mike Hommey
  (Debian bug #898332).
- Detect overwrite of -fstack-protector options with -fno-stack-protector
  (same for -fstack-protector-all and -fstack-protector-strong).
- Don't treat hexdumps which contain "cc" as compiler lines; reported by Kurt
  Roeckx (Debian bug #899137).


Version 0.08
------------

- Support new dpkg versions which replaced Dpkg::Arch's debarch_to_debtriplet
  with debarch_to_debtuple (Debian Bug #844393), reported by Johannes Schauer.
- Support Open MPI mpicc/mpicxx compiler wrappers to prevent false positives
  in non-verbose-build detection, reported by Boud Roukema and Nico Schlömer
  (Debian Bug #853265).
- Add better support for Fortran (c.f. Debian Bug #853265).
- Don't report missing PIE flags in buildd mode if GCC defaults to PIE (c.f.
  Debian Bug 845339).
- Add new --debian option to handle PIE flags like buildd mode, thanks to
  Eriberto Mota for the suggestion. This is not enabled per default to prevent
  false negatives as the flags are missing from the build log and blhc can't
  detect if the compiler applied PIE internally (c.f. Debian Bug 845339).
- Add --line-numbers command line option
- Sync architecture specific hardening support with dpkg 1.19.0.5.
- Use proper look back for non-verbose detection if DEB_BUILD_OPTIONS=parallel
  is present. Previously it was too small causing false-positives if the
  option was detected.


Version 0.07
------------

- Sync architecture specific hardening support with dpkg 1.18.10.
- Fix false positive in "gcc > file" (Debian Bug #828789), reported by Mathieu
  Parent.
- Fix another Ada false positive for format flags (Debian Bug #833939),
  reported by Nicolas Boulenguez.


Version 0.06
------------

- Sync architecture specific hardening support with dpkg 1.18.7.
- Fix false positive in "libtool: link: g++ -include test.h .." (Debian Bug
  #784959), reported by Raphaël Hertzog.
- Fix false positive with `gcc -v` (Debian Bug #765756), reported by Andreas
  Beckmann.
- Fix false positive in `rm` lines (Debian Bug #772853), reported by Jakub
  Wilk.
- Update t/tests.t for new output of Pod::Usage in 1.65 (Debian Bug #825428),
  reported by Niko Tyni, patch by Gregor Herrmann.
- Fix false positives for comment lines (Debian Bug #825671), reported by
  Fabian Wolff.
- Improve non-verbose detection for parallel builds (Debian Bug #801492),
  reported by Mattia Rizzolo, initial patch by Julien Lamy.


Version 0.05
------------

- Fix false positive in configure output if $CC contains options (Debian bug
  #710135), reported by Bastien Roucariès.
- Handle another case of Qt's `moc` (Debian bug #710780), reported by Felix
  Geyer.
- Fix detection of build dependencies for buildd logs (Debian bug #719656),
  reported by Nicolas Boulenguez.
- Fix buildd architecture detection. Only relevant if the chroot setup fails
  and dpkg-buildpackage is never run; therefore a minor issue.
- Fix false positive when "compiling" python files (Debian bugs #714630 and
  #753080), reported by Matthias Klose, patch by James McCoy.
- Don't check for hardening flags in non-verbose compiler commands spanning
  multiple lines.
- Better handling of libtool commands (Debug bug #717598), reported by Stefan
  Fritsch.

- Sync architecture specific hardening support with dpkg 1.17.13.
- Check for -fstack-protector-strong on supported platforms (since dpkg
  1.17.11) (Debian bug #757885), reported by Markus Koschany.
- Consider lines with -O0 or -Og debug builds and disable checks for -O2
  (Debian bug #714628), reported by Matthias Klose. Also don't check for
  fortification in those lines as it requires optimization (Debian bug
  #757683), also reported by Matthias Klose.


Version 0.04
------------

- Fix many false positives, this includes compiled header files, lines with
  only CC=gcc but no other compiler commands and `moc-qt4`/`moc-qt5` commands.
- Accept -Wformat=2 because it implies -Wformat.
- Accept --param ssp-buffer-size=4 (space instead of equals sign).
- Fix build dependency related checks (Ada, hardening-wrapper) for pbuilder
  build logs.
- Fix architecture detection in old buildd build logs which use an additional
  "is" in the "dpkg-buildpackage: host architecture" field.

- Updated output in buildd mode.
- Only return non-zero exit codes for errors in buildd mode, not for warnings.
- Minor performance improvements.
- Support for Ada files.


Version 0.03
------------

- Fix --ignore-flag with -fPIE.
- Detect overwrite of -D_FORTIFY_SOURCE=2 with -D_FORTIFY_SOURCE=0 or 1 or
  -U_FORTIFY_SOURCE.

- Add --ignore-arch-flag and --ignore-arch-line options to ignore flags and
  lines on certain architectures only.
- Buildd tags "no-compiler-commands" and "invalid-cmake-used" are now
  information ('I-') instead of warning ('W-').
- Ignore false positives when using moc-qt4.


Version 0.02
------------

- Fix --version, --help.

- Remove -Wformat-security from expected CFLAGS because it's already implied
  by -Werror=format-security (removed in dpkg-dev >= 1.16.3).


Version 0.01
------------

- Initial release.

// vim: ft=asciidoc
